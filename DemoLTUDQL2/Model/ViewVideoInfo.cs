﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DemoLTUDQL2.Model
{
    class ViewVideoInfo:DependencyObject
    {
        private static readonly DependencyProperty TitleProperty;
        private static readonly DependencyProperty PathProperty;
        private static readonly DependencyProperty DescriptionProperty;
        static ViewVideoInfo()
        {
            TitleProperty = DependencyProperty.Register("TitleProperty", typeof(string),typeof(ViewVideoInfo),new PropertyMetadata("New Video"));
            PathProperty = DependencyProperty.Register("PathProperty", typeof(string), typeof(ViewVideoInfo), new PropertyMetadata("New Image"));
            DescriptionProperty = DependencyProperty.Register("DescriptionProperty", typeof(string), typeof(ViewVideoInfo), new PropertyMetadata("New Description"));
        }
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }
        public string Path
        {
            get { return (string)GetValue(PathProperty); }
            set { SetValue(PathProperty, value); }
        }
        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
    }
}
