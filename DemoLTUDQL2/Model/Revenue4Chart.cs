﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLTUDQL2.Model
{
    class Revenue4Chart
    {
        public string Name { get; set; }
        public double Value { get; set; }
    }
}
