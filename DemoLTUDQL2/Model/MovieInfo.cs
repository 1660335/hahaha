﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace DemoLTUDQL2.Model
{
    class MovieInfo
    {
        [Key]
        public string ImdbURL { get; internal set; }
        public string Id { get; internal set; }
        public string Name { get; internal set; }
        public string Title { get; internal set; }
        public string Year { get; internal set; }
        public BitmapImage Poster { get; internal set; }
        public string PosterUri { get; internal set; }
        public string MeterRank { get; internal set; }
        public string Popularity { get; internal set; }
        public string MeterChange { get; internal set; }
        public string ContentRating { get; internal set; }
        public string Duration { get; internal set; }
        public string Genre { get; internal set; }
        public string ReleaseDate { get; internal set; }
        public string Rating { get; internal set; }
        public string Votes { get; internal set; }
        public string UserReviews { get; internal set; }
        public string CriticReviews { get; internal set; }
        public string Metascore { get; internal set; }
        public string Metacritic { get; internal set; }
        public string Description { get; internal set; }
        public string Director { get; internal set; }
        public string Writers { get; internal set; }
        public string Cast { get; internal set; }
        public string Awards { get; internal set; }
        public Dictionary<string,decimal> Rates { get; set; }
    }
}
